<?php

require 'animal.php';
require 'ape.php';
require 'frog.php';

$sheep = new Animal("shaun");

echo 'Name : ';
echo $sheep->get_name() . "<br>"; 
echo 'Legs : ';
echo $sheep->get_legs() . "<br>"; 
echo 'Cold Blooded : ';
echo $sheep->get_cold_blooded() . "<br>"; 
echo '<br>';

$kodok = new Frog("buduk");

echo 'Name : ';
echo $kodok->get_name() . "<br>"; 
echo 'Legs : ';
echo $kodok->get_legs() . "<br>"; 
echo 'Cold Blooded : ';
echo $kodok->get_cold_blooded() . "<br>"; 
echo 'Jump : ';
$kodok->jump() ; // "hop hop"
echo '<br>';

$sungokong = new Ape("kera sakti");

echo 'Name : ';
echo $sungokong->get_name() . "<br>"; 
echo 'Legs : ';
echo $sungokong->get_legs() . "<br>"; 
echo 'Cold Blooded : ';
echo $sungokong->get_cold_blooded() . "<br>"; 
echo 'Yell : ';
$sungokong->yell();

?>