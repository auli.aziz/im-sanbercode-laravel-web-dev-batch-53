@extends('layouts.master')

@section('judul', 'SanberBook')

@section('content')

<header>
    <h2>Social Media Deveoper Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
  </header>
  <main>
    <section>
      <h3>Benefit Join di SanberBook</h3>
      <ul>
        <li>Mendapatkan motivasi dari sesama developer</li>
        <li>Sharing knowledge dari para mastah Sanber</li>
        <li>Dibuat Oleh calon web developer terbaik</li>
      </ul>
    </section>
    <section>
      <h3>Cara bergabung ke SanberBook</h3>
      <ol>
        <li>Mengunjungi website ini</li>
        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
        <li>Selesai!</li>
      </ol>
    </section>
  </main>

@endsection