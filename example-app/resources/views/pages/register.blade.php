@extends('layouts.master')

@section('judul', 'Buat akun baru!')

@section('content') 

    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
      @csrf
      <label for="fname">Fist name:</label><br />
      <input type="text" name="fname" id="fname" /><br /><br />
      <label for="lname">Last name:</label><br />
      <input type="text" name="lname" id="lname" /><br /><br />
      <label>Gender:</label><br /> 
      <input type="radio" name="gender" value="male" />Male<br />
      <input type="radio" name="gender" value="female" />Female<br />
      <input type="radio" name="gender" value="other" />Other<br /> <br />
      <label for="nationality">Nationality:</label>
      <select name="nationality" id="nationality">
        <option value="indonesian">Indonesian</option>
        <option value="malaysian">Malaysian</option>
        <option value="australian">Australian</option>
        <option value="french">French</option>
      </select><br /> <br />
      <label>Language Spoken:</label><br /> 
      <input type="checkbox" value="bahasa ">Bahasa indonesia<br /> 
      <input type="checkbox" value="english">English<br />
      <input type="checkbox" value="french">French<br />
      <input type="checkbox" value="melayu">Melayu<br /> <br />
      <label for="bio">Bio:</label><br /> 
      <textarea name="bio" id="bio" cols="30" rows="8"></textarea><br /> <br />
      <button type="submit">Sign Up</button>
    </form>

@endsection