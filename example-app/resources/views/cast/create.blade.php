@extends('layouts.master')

@section('judul', 'Halaman Tambah Cast')

@section('content')

<form action="/cast" method="POST">
  @csrf
  <div class="form-group">
    <label class="form-label">Nama</label>
    <input type="text" class="form-control" name="nama">
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label class="form-label">Umur</label>
    <input type="number" class="form-control" name="umur">
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label class="form-label">Bio</label>
    <input type="text" class="form-control" name="bio">
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection