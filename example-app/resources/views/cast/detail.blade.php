@extends('layouts.master')

@section('judul', 'Detail Cast')

@section('content')

<h1 class="text-primary">{{$cast->nama}}</h1>
<p class="text-secondary">Umur: {{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

@endsection