@extends('layouts.master')

@section('judul', 'Edit Cast')

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
  @csrf
  @method('PUT')
  <div class="form-group">
    <label class="form-label">Nama</label>
    <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label class="form-label">Umur</label>
    <input type="number" class="form-control" name="umur" value="{{$cast->umur}}">
    @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <div class="form-group">
    <label class="form-label">Bio</label>
    <textarea type="text" class="form-control" name="bio" cols="30" rows="10">{{$cast->bio}}</textarea>
    @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection